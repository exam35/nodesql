const express=require('express')
const cors=require('cors')
const mysql=require('mysql2')


const connection=mysql.createConnection({
    // host:'localhost',
    host:'172.17.0.2',
    user:'root',
    password:'root',
    waitForConnections:true,
    // connectionLimit:10,
    database:'employee'
})


const app=express()
app.use(cors('*'))
app.use(express.json())
connection.connect()

////////////////////////////////////////////

app.get('/',(req,res)=>{
    connection.query('select * from employee',(error,result)=>{
        if(error!=null)
            res.send(error)
        else
            res.send(result)    
    })
    
})

app.post('/',(req,res)=>{
    const query=`insert into employee values(default,'${req.body.name}','${req.body.address}',${req.body.salary})`
    connection.query(query,(error,result)=>{
        if(error!=null)
            res.send(error)
        else
            res.send(result)  
    })
})
app.put('/:no',(req,res)=>{
    const query=`update employee set name='${req.body.name}',address='${req.body.address}',salary=${req.body.salary} where id=${req.params.no}`;
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)  
    })
})

app.delete('/:no',(req,res)=>{
    const query=`delete from employee where id=${req.params.no}`
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)  
    })
})

////////////////////////////////////////
app.listen(3000,'0.0.0.0',()=>{
    console.log('server started at 3000   :)' );
})